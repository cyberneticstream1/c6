"use client";
import { Inter } from '@next/font/google';
import "./globals.css"
import React from "react";
import BgRectangles from "../components/BgRectangles.js";
import Map from "../components/map.js";
import { useUserAgent } from 'next-useragent';

const inter = Inter({
    variable: '--font-inter',
});
export default function RootLayout({children}) {

    const [uA, setUa] = React.useState({})

  React.useEffect(() => {
        setUa(useUserAgent(window.navigator.userAgent))
    },[])

    // @ts-ignore
    return (
            <html lang="en" className={`${inter.variable} font-sans`}>
                <head />
                <body>
                    <BgRectangles mobileVisibility = {"block"} pCVisibility={"hidden"}/>
                    <Map  dLat = {-0.09} dLng={0.15}/>
                    {children}
                </body>

            </html>
            )
}
